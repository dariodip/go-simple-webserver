package main

import (
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"TodoServer/data"
	"TodoServer/routes"
	"log"
)

func init() {
	log.Println("Initializing DB")
	data.InitDb()
}

func main() {
	log.Println("Creating Default Router")
	var router = routes.Router
	router.Static("/css", "./public/css")
	router.LoadHTMLGlob("templates/*")

	log.Println("Server started")
	router.Run(":8080")
	log.Println("Server listening on 8080")
}
