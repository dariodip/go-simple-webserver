package models

import "github.com/jinzhu/gorm"

type Person struct {
	gorm.Model
	Id      uint   `gorm:"primarykey" json:"id"`
	Name    string `json:"name"`
	Surname string `json:"surname"`
	CityId  uint   `json:"city_id"`
}

type PersonWithCity struct {
	Id       uint   `json:"id"`
	Name     string `json:"name"`
	Surname  string `json:"surname"`
	CityId   uint   `json:"city_id"`
	CityName string `json:"city_name"`
	PostCode uint   `json:"post_code"`
	Region   string `json:"region"`
}

type PersonJSON struct {
	Id      uint   `json:"id"`
	Name    string `json:"name"`
	Surname string `json:"surname"`
	CityId  uint   `json:"city_id"`
}

func (Person) TableName() string {
	return "person"
}

func (Person) Find(db *gorm.DB) []DBModel {
	var results []Person
	db.Find(&results)
	var res = make([]DBModel, len(results), len(results))
	for i, v := range results {
		res[i] = v
	}
	return res
}

func (Person) FindOne(id uint, db *gorm.DB) DBModel {
	var result Person
	db.Find(&result, id)
	var res DBModel
	res = result
	return res
}

func (Person) ToPerson(dbmodel DBModel) Person {
	return dbmodel.(Person)
}

func (Person) ToPersons(dbmodels []DBModel) []Person {
	persons := make([]Person, len(dbmodels), len(dbmodels))
	for i, v := range dbmodels {
		persons[i] = v.(Person)
	}
	return persons
}

func GetUsersWithCity(users []Person, cities []City) []PersonWithCity {
	var usersWithCities = make([]PersonWithCity, 0, len(users))
	mappedCities := mapCities(&cities)
	for _, user := range users {
		var cityId = user.CityId
		var city = mappedCities[cityId]
		usersWithCities = append(usersWithCities, PersonWithCity{Id: user.ID,
			Name:     user.Name,
			Surname:  user.Surname,
			CityId:   user.CityId,
			CityName: city.Name,
			PostCode: city.PostCode,
			Region:   city.Region,
		})
	}
	return usersWithCities
}
