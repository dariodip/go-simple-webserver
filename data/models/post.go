package models

import (
	"TodoServer/data/external"
	"encoding/json"
)

const POSTURL = external.URLPREFIX + "/posts"

type Post struct {
	UserId uint   `json:"userId"`
	Id     uint   `json:"id"`
	Title  string `json:"title"`
	Body   string `json:"body"`
}

func (Post) GetAll() []Post {
	var postssString = external.HttpGetReq(POSTURL)
	var result []Post
	json.Unmarshal([]byte(postssString), &result)

	return result
}
