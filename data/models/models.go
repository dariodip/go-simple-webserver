package models

import "github.com/jinzhu/gorm"

type DBModel interface {
	Find(db *gorm.DB) []DBModel
	FindOne(id uint, db *gorm.DB) DBModel
}
