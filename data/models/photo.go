package models

import (
	"TodoServer/data/external"
	"encoding/json"
)

const PHOTOURL = external.URLPREFIX + "/photos"

type Photo struct {
	AlbumId      uint   `json:"albumId"`
	Id           uint   `json:"id"`
	Title        string `json:"title"`
	Url          string `json:"url"`
	ThumbnailUrl string `json:"thumbnailUrl"`
}

func (Photo) GetAll() []Photo {
	var photosString = external.HttpGetReq(PHOTOURL)
	var result []Photo
	json.Unmarshal([]byte(photosString), &result)

	return result
}

func mapPhotos(photos *[]Photo) map[uint][]Photo {
	photoMap := map[uint][]Photo{}

	for _, photo := range *photos {
		var photoWithAlbumId = photoMap[photo.AlbumId]
		photoMap[photo.AlbumId] = append(photoWithAlbumId, photo)
	}
	return photoMap
}
