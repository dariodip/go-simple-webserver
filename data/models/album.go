package models

import (
	"TodoServer/data/external"
	"encoding/json"
)

const ALBUMURL = external.URLPREFIX + "/albums"

type Album struct {
	UserId uint   `json:"userId"`
	Id     uint   `json:"id"`
	Title  string `json:"title"`
}

type AlbumWithPhotos struct {
	UserId uint    `json:"userId"`
	Id     uint    `json:"id"`
	Title  string  `json:"title"`
	Photos []Photo `json:"photos"`
}

func (Album) GetAll() []Album {
	var albumsString = external.HttpGetReq(ALBUMURL)
	var result []Album
	json.Unmarshal([]byte(albumsString), &result)

	return result
}

func (Album) GetAlbumsWithPhotos() []AlbumWithPhotos {
	var (
		albumResult         []Album
		photoResult         []Photo
		firstReq, secondReq external.HttpRequestResult
	)

	ch := make(chan external.HttpRequestResult)
	defer close(ch)

	go external.MakeRequest(PHOTOURL, ch)
	go external.MakeRequest(ALBUMURL, ch)

	firstReq, secondReq = <-ch, <-ch
	if firstReq.Url == PHOTOURL {
		json.Unmarshal([]byte(firstReq.Data), &photoResult)
	} else if firstReq.Url == ALBUMURL {
		json.Unmarshal([]byte(firstReq.Data), &albumResult)
	}
	if secondReq.Url == PHOTOURL {
		json.Unmarshal([]byte(secondReq.Data), &photoResult)
	} else if secondReq.Url == ALBUMURL {
		json.Unmarshal([]byte(secondReq.Data), &albumResult)
	}

	var result = make([]AlbumWithPhotos, 0, len(albumResult))
	mappedPhotos := mapPhotos(&photoResult)
	mappedAlbums := mapAlbums(&albumResult)

	for albumId, album := range mappedAlbums {
		var photos = mappedPhotos[albumId]
		var albumWPhoto = AlbumWithPhotos{Id: album.Id, Title: album.Title, UserId: album.UserId, Photos: photos}
		result = append(result, albumWPhoto)
	}

	return result
}

func mapAlbums(albums *[]Album) map[uint]Album {
	albumsMap := map[uint]Album{}

	for _, album := range *albums {
		albumsMap[album.Id] = album
	}
	return albumsMap

}
