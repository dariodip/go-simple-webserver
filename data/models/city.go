package models

import (
	"github.com/jinzhu/gorm"
)

type City struct {
	gorm.Model
	Id       uint   `gorm:"primarykey" json:"id"`
	Name     string `json:"name"`
	PostCode uint   `json:"post_code"`
	Region   string `json:"region"`
}

type CityJSON struct {
	Id       uint   `json:"id"`
	Name     string `json:"name"`
	PostCode uint   `json:"post_code"`
	Region   string `json:"region"`
}

func (City) TableName() string {
	return "city"
}

func (City) Find(db *gorm.DB) []DBModel {
	var results []City
	db.Find(&results)
	var res = make([]DBModel, len(results), len(results))
	for i, v := range results {
		res[i] = v
	}
	return res
}

func (City) FindOne(id uint, db *gorm.DB) DBModel {
	var result City
	db.Find(&result, id)
	var res DBModel
	res = result
	return res
}

func (City) ToCity(dbmodel DBModel) City {
	return dbmodel.(City)
}

func (City) ToCities(dbmodels []DBModel) []City {
	cities := make([]City, len(dbmodels), len(dbmodels))
	for i, v := range dbmodels {
		cities[i] = v.(City)
	}
	return cities
}

func mapCities(cities *[]City) map[uint]City {
	cityMap := map[uint]City{}

	for _, city := range *cities {
		cityMap[city.ID] = city
	}
	return cityMap
}
