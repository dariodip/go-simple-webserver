package external

import (
	"io/ioutil"
	"net/http"
)

const URLPREFIX = "https://jsonplaceholder.typicode.com"

type HttpRequestResult struct {
	Data string
	Url  string
}

func HttpGetReq(url string) string {
	rs, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer rs.Body.Close()

	bodyBytes, err := ioutil.ReadAll(rs.Body)
	if err != nil {
		panic(err)
	}
	return string(bodyBytes)
}

func MakeRequest(url string, ch chan<- HttpRequestResult) {
	httpReq := HttpGetReq(url)
	res := HttpRequestResult{Data: httpReq, Url: url}
	ch <- res
}
