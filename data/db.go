package data

import (
	"TodoServer/data/models"
	"github.com/jinzhu/gorm"
)

var DB *gorm.DB

func InitDb() {
	var err error
	DB, err = gorm.Open("sqlite3", "data/DB.sqlitedb")
	if err != nil {
		panic(err)
	}
	DB.AutoMigrate(&models.City{}, &models.Person{})
	DB.Model(&models.Person{}).AddForeignKey("city_id", "city(id)", "RESTRICT", "RESTRICT")

}
