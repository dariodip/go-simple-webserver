# Simple Server

Simple Server is just a dummy implementation of a restful service, in order to check how all these tools work.
The server is fully developed using Golang as the language, gin-gonic as the web framework, gorm as the ORM and SQLite as the db.

## API

I developed the same API both on [Python's Flask](http://flask.pocoo.org/) and in [Node.js' Express](http://expressjs.com/) for a project for the University.
You can found the repo at https://github.com/dariodip/wsbenchmarking.

The WS uses an SQLite database preloaded with data generated from [Generate Data](https://www.generatedata.com/). The db has the following tables:

- `city(id, name, post_code, region)`: contains 300 cities;
- `person(id, name, surname, city_id)`: contains 1000 people. `city_id` is the foreign key for `id` of the table `city`.

The WS retrieves data from the external source [JSONPlaceholder](https://jsonplaceholder.typicode.com/). The retrieved data have the following schema:

- `post(userId, id, title, body)`;
- `album(userId, id, title)`;
- `photo(albumId, id, title, url, thumbnailUrl)`.

The WS has the following endpoints:

- `/hello_world`: a simple HTML page showing greetings;
- `/hello/<user>`: an HTML page showing customized greetings for the user;
- `/fib30`: an HTML page showing the 30th Fibonacci number;
- `/single_user`: an HTML page showing informations about a single user in the database;
- `/all_users`: an HTML page showing informations about all the users;
- `/users_with_cities`: an HTML page showing informations about all the user with their cities;
- `/api/hello_world`: a JSON containing greetings;
- `/api/hello/<user>`: a JSON containing customized greetings for the user;
- `/api/fib30`: a JSON containing the 30th Fibonacci number;
- `/api/single_user`: a JSON containing informations about a single user in the database;
- `/api/all_users`: a JSON containing informations about all the users;
- `/api/users_with_cities`: a JSON containing informations about all the user with their cities;
- `/external/posts`: a JSON containing information about all the posts retrieved from the external source;
- `/external/albums`: a JSON containing information about all the albums retrieved from the external source;
- `/external/photos`: a JSON containing information about all the photos retrieved from the external source;
- `/external/albums_w_photos`: a JSON containing information about all the albums, with the photos it contains, retrieved from the external source;

I've included a collection of Postman v2 test in the `test` folder.

## Setup

After cloning the repo, run `go get ./...` in the main folder then `go build .`.

## Run

After the Setup step, run `go run app.go`. Have fun

## Contributing

Feel free to contribute to the project, open issues and so on.