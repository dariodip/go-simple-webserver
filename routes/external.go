package routes

import (
	"TodoServer/data/models"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

var ExtRouter = Router.Group("external")

func init() {
	log.Println("Initializing External router")
	ExtRouter.GET("posts", getPosts)
	ExtRouter.GET("albums", getAlbums)
	ExtRouter.GET("photos", getPhotos)
	ExtRouter.GET("albums_w_photos", getAlbumsWPhotos)
	log.Println("External router initializzation done")
}

func getPosts(c *gin.Context) {
	result := models.Post{}.GetAll()
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": result})
}

func getAlbums(c *gin.Context) {
	result := models.Album{}.GetAll()
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": result})
}

func getPhotos(c *gin.Context) {
	result := models.Photo{}.GetAll()
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": result})
}

func getAlbumsWPhotos(c *gin.Context) {
	result := models.Album{}.GetAlbumsWithPhotos()
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": result})
}
