package routes

import (
	"TodoServer/data"
	"TodoServer/data/models"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

var PagesRouter = Router.Group("")

func init() {
	log.Println("Initializing page router")
	PagesRouter.GET("hello_world", helloWorld)
	PagesRouter.GET("hello/:user", helloUser)
	PagesRouter.GET("fib30", fib30page)
	PagesRouter.GET("single_user", singleUser)
	PagesRouter.GET("/all_users", getAllPeoplePage)
	PagesRouter.GET("/users_with_cities", allUsersWCities)
	log.Println("Pages router initialization done")
}

func helloWorld(c *gin.Context) {

	c.HTML(http.StatusOK, "hello_world.tmpl", gin.H{})

}

func helloUser(c *gin.Context) {

	var name = c.Param("user")
	c.HTML(http.StatusOK, "hello_name.tmpl", gin.H{"name": name})

}

func fib30page(c *gin.Context) {

	var fib30 = fib(30)
	c.HTML(http.StatusOK, "fib30.tmpl", gin.H{"fib": fib30})

}

func singleUser(c *gin.Context) {
	var DB = data.DB
	var singleUser models.Person

	DB.First(&singleUser)

	c.HTML(http.StatusOK, "single_user.tmpl", gin.H{"user": singleUser})
}

func getAllPeoplePage(c *gin.Context) {
	var DB = data.DB
	var users []models.Person

	DB.Find(&users)

	c.HTML(http.StatusOK, "all_users.tmpl", gin.H{"users": users})
}

func allUsersWCities(c *gin.Context) {
	var citiesModel []models.DBModel
	var usersModel []models.DBModel

	citiesModel = models.City{}.Find(data.DB)
	usersModel = models.Person{}.Find(data.DB)

	cities := models.City{}.ToCities(citiesModel)
	users := models.Person{}.ToPersons(usersModel)

	usersWithCities := models.GetUsersWithCity(users, cities)

	c.HTML(http.StatusOK, "all_users_w_city.tmpl", gin.H{"users": usersWithCities})
}
