package routes

import (
	"TodoServer/data"
	"TodoServer/data/models"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

var APIRouter = Router.Group("api")

func init() {
	log.Println("Initializing API router")
	APIRouter.GET("hello_world", helloWorldApi)
	APIRouter.GET("hello/:user", helloUserApi)
	APIRouter.GET("fib30", fib30Api)
	APIRouter.GET("single_user", singleUserApi)
	APIRouter.GET("/all_users", getAllPeople)
	APIRouter.GET("/users_with_cities", allUsersWCitiesApi)
	log.Println("API router initialization done")
}

func helloWorldApi(c *gin.Context) {
	helloJson := `{"hello": "world"}`
	var result map[string]string
	json.Unmarshal([]byte(helloJson), &result)

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": result})
}

func helloUserApi(c *gin.Context) {
	name := c.Param("user")
	helloJson := fmt.Sprintf(`{"hello": "%s"}`, name)
	var result map[string]string
	json.Unmarshal([]byte(helloJson), &result)

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": result})
}

func fib30Api(c *gin.Context) {

	fib30Json := fmt.Sprintf(`{"fib30": "%d"}`, fib(30))
	var result map[string]string
	json.Unmarshal([]byte(fib30Json), &result)

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": result})
}

func allUsersWCitiesApi(c *gin.Context) {
	var citiesModel []models.DBModel
	var usersModel []models.DBModel

	citiesModel = models.City{}.Find(data.DB)
	usersModel = models.Person{}.Find(data.DB)

	cities := models.City{}.ToCities(citiesModel)
	users := models.Person{}.ToPersons(usersModel)

	usersWithCities := models.GetUsersWithCity(users, cities)

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": usersWithCities})

}

func singleUserApi(c *gin.Context) {

	singleUserModel := models.Person{}.FindOne(uint(1), data.DB)
	singleUser := models.Person{}.ToPerson(singleUserModel)

	jsonPerson := models.PersonJSON{Id: singleUser.ID,
		Name:    singleUser.Name,
		Surname: singleUser.Surname,
		CityId:  singleUser.CityId}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": jsonPerson})
}

func getAllPeople(c *gin.Context) {

	var peopleModel = models.Person{}.Find(data.DB)
	var people = models.Person{}.ToPersons(peopleModel)

	if len(people) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No person found!"})
		return
	}

	jsonPeople := make([]models.PersonJSON, 0, len(people))

	for _, person := range people {
		jsonPeople = append(jsonPeople, models.PersonJSON{Id: person.ID,
			Name:    person.Name,
			Surname: person.Surname,
			CityId:  person.CityId})
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": jsonPeople})

}

func fib(n int) int {
	if n == 0 {
		return 1
	} else if n == 1 {
		return 1
	} else {
		return fib(n-1) + fib(n-2)
	}
}
